### Lottozahlengenerator

Generates a quick tip for the lotto game at [win2day.at](http://win2day.at)

Check out the my page at [flashfreezer.net](http://flashfreezer.net)

#### Anwendung (.jar)
	ausfuehrbar in der Konsole/Terminal mit:	java -jar lotto.jar


 
#### License
	Copyright (C) 2012  Matthias Lexer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

