//-------------------------------------------------------------------------------------//
//
//	Name:		Matthias Lexer
//	License: 	GNU GPL v3
//
//	Version: 	0.3	Beta
// 	Date:		07.11.2012
//	
//	Description:	console version with color - marked - tip 
//					ONLY USE WITH MAC OR LINUX CONSOLE/TERMINAL
//					
//
//-------------------------------------------------------------------------------------//


package lotto.zahlengenerator;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.io.BufferedReader;


public class lotto {

	public static void main(String[] args) throws IOException {
		System.out.println("Wie viele Tipps wollen sie [1-10]: ");
		
		//-----------------------------Eingabe auswerten--------------------------------//
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		String xtimes = null;
		try {
			xtimes = console.readLine();
			} catch (IOException e) {
				// Sollte eigentlich nie passieren
				e.printStackTrace();
			}
		System.out.println("Hier sind ihre " + xtimes + " Lottotipps");
		System.out.println();
		
		//------------------Erstellung der Tipps je nachdem wie gew�hlt------------------/
		int xtimesINT = Integer.parseInt(xtimes);
		
		if(xtimesINT>0) {	lotto lotto1 = new lotto();	
							lotto1.LottoInit();
						}
		if(xtimesINT>1) {	lotto lotto2 = new lotto();	
							lotto2.LottoInit();
						}
		if(xtimesINT>2) {	lotto lotto3 = new lotto();	
							lotto3.LottoInit();
						}
		if(xtimesINT>3) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>4) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>5) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>6) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>7) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>8) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>9) {	lotto lotto4 = new lotto();	
							lotto4.LottoInit();
						}
		if(xtimesINT>10){	
							System.out.println("Die maximale Anzahl betr�gt 10 Tipps, 10 Tipps wurden erstellt.");
						}
		System.out.println("\033[0;32m (c)by Matthias Lexer (flashfreezer@gmail.com) \033[0m");
	}
		
	public void LottoInit(){
		
		//erstellt den Lottozettel und beschreibt ihn
		
		String[][] lottozettel = new String[8][6];
		int iNumber = 1;
		
		//-------------------------Zufallszahlen----------------------//
		int[] zufallszahlen = new int[5]; 
		Random random = new Random();
		int iZufall;
		
		for(int i = 0; i<zufallszahlen.length; i++){
		     do {
		    	 iZufall = random.nextInt(45)+1;
		     }
		     while(CheckIfInArray(zufallszahlen, iZufall));
		     
		     zufallszahlen[i]=iZufall;
		}
		//-----------------------------------------------------------//
		boolean bZahlistZufallszahl = false;
		
		for (int zeilen = 0; zeilen < 8; zeilen++) {
			for (int spalten = 0; spalten < 6; spalten++) {
				String s = String.valueOf(iNumber);
				if(!bZahlistZufallszahl){
					for(int k=0; k<5; k++) {			//zum "Durchgehen" des Arrays
						if(iNumber == zufallszahlen[k]){	
							lottozettel[zeilen][spalten] = "\033[1;36m"+s+"\033[0m";
							//	\033[1;36m		<--		blau
							//	\033[0m			<-- 	farblos
							bZahlistZufallszahl = true;
						}
					}
				}
				if(!bZahlistZufallszahl){
					lottozettel[zeilen][spalten] = "\033[0m"+s;
				}
				
				bZahlistZufallszahl = false;
				iNumber++;
				if (iNumber == 46)	WriteOnSheet(lottozettel);	//wenn zahl 46 erreicht dann soll er schreiben
			}
		}
		}

	public boolean CheckIfInArray(int[] array,int number)		//Schaut ob die Zahl schon im Array vorhanden ist
	{
		boolean fail = false;
		 for(int j=0; j<array.length; j++){
	    	 if(number == array[j]) fail = true;
	     }
		 
		 if(fail) return true;
		 else return false;
	}
	
	public void WriteOnSheet(String[][] array) 		//Zeichnen des Lottozettels
	{
		//ACHTUNG RICHTIGE AUSGABE NUR UNTER LINUX UND MAC GETESTET (KONSOLE)
		//UNTER ECLIPSE WIRD DER ASCII FARBCODE NICHT UMGESETZT
		for (int zeilen = 0; zeilen < 8; zeilen++) 
		{
			for (int spalten = 0; spalten < 6; spalten++) 
			{
				if(array[zeilen][spalten]!=null)
				{
					System.out.print(array[zeilen][spalten]);
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
	}
}
